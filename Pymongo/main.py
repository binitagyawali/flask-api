from flask import Flask, request, Response
import pymongo
import json


app = Flask(__name__)
client = pymongo.MongoClient("mongodb+srv://m001-student:m001-mongodb-basics@sandbox.ok97i.mongodb.net/")
db = client.api
dt = db["task"]

@app.route('/')
def hello_fusemachines():
   return 'Hello Fusemachines!'


@app.route('/item/new', methods = ['POST'])
def add_item():
   #Get item from the POST body
   req_data = request.get_json()
   item = req_data['item']
   
   #Add item to the list
   mydict = {"item": item, "status": "ongoing"}
   res_data = dt.insert_one(mydict)
   #Return error if item not added
   if res_data is None:
      response = Response("{'error': 'Item not added - '}"  + item, status=400 , mimetype='application/json')
      return response
   
   #Return response
   response = Response(json.dumps(res_data), mimetype='application/json')
   
   return response


@app.route('/items/all')
def get_all_items():

   # Get items from the helper
   res_data = list(dt.find())

   #Return response
   response = Response(json.dumps(res_data), mimetype='application/json')
   return response


# @app.route('/item/status', methods=['GET'])
# def get_item():
#
#    #Get parameter from the URL
#    item_name = request.get_json().get('name')
#
#    # Get items from the helper
#    res_data = list(dt.find_one())
#    status = {"item" : res_data}
#
#    #Return 404 if item not found
#    if status is None:
#       response = Response("{'error': 'Item Not Found - '}"  + item_name, status=404 , mimetype='application/json')
#       return response
#
#    #Return status
#    res_data = status
#
#    response = Response(json.dumps(res_data), status=200, mimetype='application/json')
#    return response


@app.route('/item/update', methods = ['PUT'])
def update_status():
   #Get item from the POST body
   req_data = request.get_json()
   item = req_data['item']
   status = req_data['status']
   
   #Update item in the list
   myquery = {"item": item, "status": status}
   newvalues = {"$set": {"item": item, "status": status}}

   dt.update_one(myquery, newvalues)
   res_data = list(dt.find())

   if res_data is None:
      response = Response("{'error': 'Error updating item - '" + item + ", " + status   +  "}", status=400 , mimetype='application/json')
      return response
   
   #Return response
   response = Response(json.dumps(res_data), mimetype='application/json')
   
   return response


@app.route('/item/remove', methods = ['DELETE'])
def delete_item():
   #Get item from the POST body
   req_data = request.get_json()
   item = req_data['item']
   
   #Delete item from the list
   myquery = {"item": item}
   dt.delete_one(myquery)
   res_data = {'item': item}
   if res_data is None:
      response = Response("{'error': 'Error deleting item - '" + item +  "}", status=400 , mimetype='application/json')
      return response
   
   #Return response
   response = Response(json.dumps(res_data), mimetype='application/json')
   
   return response


if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=True)
