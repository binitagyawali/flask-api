import helper
from flask import Flask, request, Response
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import json


app = Flask(__name__)
engine = create_engine("postgresql://postgres:binita22@localhost:5432/api")
db = scoped_session(sessionmaker(bind=engine))

@app.route('/')
def hello_fusemachines():
   return 'Hello Fusemachines!'


@app.route('/item/new', methods = ['POST'])
def add_item():
   #Get item from the POST body
   req_data = request.get_json()
   item = req_data['item']
   
   #Add item to the list
   db.execute(f"INSERT INTO task (item, status) VALUES ('{item}', 'Nothing');")
   db.commit()
   result_set = db.execute("SELECT * FROM task;")
   results = list(result_set)
   results = [n[1:] for n in results]
   res_data = {'item': results}
   #Return error if item not added
   if res_data is None:
      response = Response("{'error': 'Item not added - '}"  + item, status=400 , mimetype='application/json')
      return response
   
   #Return response
   response = Response(json.dumps(res_data), mimetype='application/json')
   
   return response


@app.route('/items/all')
def get_all_items():

   # Get items from the helper
   result_set = db.execute("SELECT * FROM task;")
   results = list(result_set)
   results = [n[1:] for n in results]
   res_data = {'item': results}

   #Return response
   response = Response(json.dumps(res_data), mimetype='application/json')
   return response


@app.route('/item/status', methods=['GET'])
def get_item():

   #Get parameter from the URL
   item_name = request.get_json().get('name')
   
   # Get items from the helper
   result_set = db.execute(f"SELECT * FROM task WHERE item = '{item_name}';")
   result = list(result_set)
   result = [n[1:] for n in result]
   status = {"item" : result}
   
   #Return 404 if item not found
   if status is None:
      response = Response("{'error': 'Item Not Found - '}"  + item_name, status=404 , mimetype='application/json')
      return response

   #Return status
   res_data = status

   response = Response(json.dumps(res_data), status=200, mimetype='application/json')
   return response


@app.route('/item/update', methods = ['PUT'])
def update_status():
   #Get item from the POST body
   req_data = request.get_json()
   item = req_data['item']
   status = req_data['status']
   
   #Update item in the list
   db.execute(f"UPDATE task SET status = '{status}' WHERE item = '{item}' ;")
   db.commit()

   result_set = db.execute(f"SELECT * FROM task WHERE item = '{item}';")
   result = list(result_set)
   result = [n[1:] for n in result]
   res_data = {"item": result}

   if res_data is None:
      response = Response("{'error': 'Error updating item - '" + item + ", " + status   +  "}", status=400 , mimetype='application/json')
      return response
   
   #Return response
   response = Response(json.dumps(res_data), mimetype='application/json')
   
   return response


@app.route('/item/remove', methods = ['DELETE'])
def delete_item():
   #Get item from the POST body
   req_data = request.get_json()
   item = req_data['item']
   
   #Delete item from the list
   db.execute(f"DELETE FROM task WHERE item = '{item}' ;")
   db.commit()
   res_data = {'item': item}
   if res_data is None:
      response = Response("{'error': 'Error deleting item - '" + item +  "}", status=400 , mimetype='application/json')
      return response
   
   #Return response
   response = Response(json.dumps(res_data), mimetype='application/json')
   
   return response


if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=True)
